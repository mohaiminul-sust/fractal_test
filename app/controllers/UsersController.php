<?php
use Apiutil\Transformers\UsersTransformer;

class UsersController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	public function index()
	{
		// return User::all();
		return Fractal::collection(User::all(), new UsersTransformer);
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$user = User::find($id);

		if($user){
			return Fractal::item($user, new UsersTransformer);
		}	

		return Response::json([
            'error' => [
                'message' => 'Not found!',
                'status_code' => 404
            ]
        ], 404);
	}


}
