<?php 

class UserTableSeeder extends Seeder{

	public function run(){
		// User::truncate();
		DB::table('users')->delete();
		$faker = Faker\Factory::create();

	    foreach (range(1, 10) as $i) {
			User::create([
	            'username'    =>  $faker->firstname,
	            'password'    =>  Hash::make('prime'),
	         	'email'       =>  $faker->email,
	        ]);        	
	    }
	}
}