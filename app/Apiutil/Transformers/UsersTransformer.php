<?php namespace Apiutil\Transformers;

 use League\Fractal;
 // use \User;

 class UsersTransformer extends Fractal\TransformerAbstract{
 	
 	public function transform(\User $user){
 		
 		return [
 			'index' => (int) $user->id,
 			'name' => $user->username,
 			'email address' => $user->email
 		];
 	}

 }